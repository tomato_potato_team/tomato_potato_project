﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace SystemPointsManager.Core.DataBase
{
    class Group : IEntity<int>
    {
        public string Name { get; set; }
    }
}

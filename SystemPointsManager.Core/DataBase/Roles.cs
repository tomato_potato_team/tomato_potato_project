﻿using System;
using Microsoft.AspNetCore.Identity;

namespace SystemPointsManager.DataBase
{
    public class Roles: IEntity<int>
    {
        
        public String Role { get; set; }
        
    }
       
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SystemPointsManager.Core.DataBase
{
    class StudentActivity
    {
        public User StudentId { get; set; }
        public Activity ActivityId { get; set; }
        public int Point { get; set; }
        public DateTime DateOfActivity { get; set; }
        public Lessons LessonId { get; set; }
    }
}

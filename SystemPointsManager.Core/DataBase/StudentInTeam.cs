﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SystemPointsManager.Core.DataBase
{
    class StudentInTeam
    {
        public User StudentId { get; set; }
        public Team TeamId { get; set; }
        public DateTime DateIn { get; set; }
    }
}

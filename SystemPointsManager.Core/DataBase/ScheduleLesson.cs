﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SystemPointsManager.Core.DataBase
{
    class ScheduleLesson
    {
        public User SpeakerId { get; set; }
        public DateTime Date { get; set; }
        public Lessons LessonId { get; set; }
        public StudentOfGroup StudentOfGroupId { get; set; }
        public bool RequiredHomeWork { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace SystemPointsManager.Core.DataBase
{
    class StudentOfGroup : IEntity<int>
    {
        public Group GroupId { get; set; }
        public User StudentId { get; set; }

    }
}

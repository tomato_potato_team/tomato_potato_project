﻿using System;
using Microsoft.AspNetCore.Identity;

namespace SystemPointsManager.DataBase
{
    public class User: IEntity <int>
    {
        /// <summary>
        /// Персональные данные
        /// </summary>
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String PatronymicName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public String Sex { get; set; }
        public Roles RoleId { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }

    }
}

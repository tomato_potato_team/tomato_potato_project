﻿using System;


namespace SystemPointsManager.DataBase
{
    public class Course
    {

        public String CourseName { get; set; }
        public DateTime CoursePeriod { get; set; }

    }

}
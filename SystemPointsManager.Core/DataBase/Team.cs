﻿using System;
using Microsoft.AspNetCore.Identity;

public class Team : IEntity<int>
{
    public String TeamName { get; set; }
    public User MenthorId { get; set; }
    public String ProjectTheme { get; set; }
    public Course CourseId { get; set; }

}